const express = require("express");
const path = require("path");
const app = express();
const port = 3168;

app.use(express.static("public"), express.static("dist"));
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
});
app.get("/table", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/table.html"));
});
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
